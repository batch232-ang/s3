package com.zuitt.example;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Activity {
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);

        ArrayList<String> topGames = new ArrayList<>();

        HashMap<String, Integer> games = new HashMap<>();
        games.put("Mario Odyssey", 50);
        games.put("Super Mario Bros. Ultimate", 20);
        games.put("Luigi's Mansion 3", 15);
        games.put("Pokemon Sword", 30);
        games.put("Pokemon Shield", 100);

        games.forEach((key, value) -> {
            System.out.println(key + " has " + value + " stocks left.");

            if(value <= 30){
                topGames.add(key);
            }
        });

        System.out.println();
        for(String topGame: topGames){
            System.out.println(topGame + " has been added to top games list!");
        }
        System.out.println("Our shop's top games:");
        System.out.println(topGames);

        boolean addItem = true;

        while(addItem == true) {
            System.out.println();
            System.out.println("Would you like to add an item? Yes or No");
            String input = scan.nextLine();

            switch(input){
                case "Yes":
                    System.out.println("Add Item name");
                    String itemName = scan.nextLine();

                    System.out.println("Add Number of Stocks");
                    int itemStock = -1;
                    try{
                        itemStock = scan.nextInt();
                    }catch(Exception e){
                        e.printStackTrace();
                    }

                    if(itemStock > -1){
                        games.put(itemName, itemStock);
                        addItem = false;
                    }else{
                        System.out.println("Invalid stock value");
                    }

                    break;
                case "No":
                    System.out.println("Thank you");
                    break;
                default:
                    System.out.println("Invalid Input. Try Again.");
            }

        }
        System.out.println(games);
    }

}
